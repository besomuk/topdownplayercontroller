﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPSCounter : MonoBehaviour
{

    public int avgFrameRate;
    public Text display_Text;

    int frameInterval = 10; // provera na svakih 10 frejmova

    public void Update()
    {
        if ( Time.frameCount % frameInterval == 0 )
            CheckFPS();
    }

    private void CheckFPS()
    {
        float current = 0;
        //current = Time.frameCount / Time.time;
        current = (int)(1f / Time.unscaledDeltaTime);
        avgFrameRate = (int)current;
        display_Text.text = avgFrameRate.ToString() + " FPS";

        //print( "Total frames: " + Time.frameCount );
    }


}
