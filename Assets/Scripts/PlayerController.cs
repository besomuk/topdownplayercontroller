using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float rotationSpeed = 200;
    [SerializeField] private float moveSpeed = 6;
    [Tooltip("If on, mouse rotation script should be disabled!")]
    [SerializeField] private bool strafeOn = false;

    void Start()
    {
        Debug.Log( "Start..." );
    }
    void Update()
    {
        if ( strafeOn )
        {
            Strafe();
        }
        else
        {
            Rotate();
        }
        Move();
    }

    private void Move()
    {
        if ( Input.GetKey( KeyCode.W ) )
        {
            transform.Translate( Vector3.forward * Time.deltaTime * moveSpeed );
        }
        if ( Input.GetKey( KeyCode.S ) )
        {
            transform.Translate( Vector3.forward * Time.deltaTime * -moveSpeed );
        }
    }

    void Strafe ()
    {
        if ( Input.GetKey( KeyCode.A ) )
        {
            transform.Translate( Vector3.left * Time.deltaTime * moveSpeed );
        }
        if ( Input.GetKey( KeyCode.D ) )
        {
            transform.Translate( Vector3.right * Time.deltaTime * moveSpeed );
        }
    }

    void Rotate()
    {
        if ( Input.GetKey( KeyCode.A ) )
        {
            transform.Rotate( new Vector3( 0, 1, 0 ) * Time.deltaTime * -rotationSpeed );
        }
        if ( Input.GetKey( KeyCode.D ) )
        {
            transform.Rotate( new Vector3( 0, 1, 0 ) * Time.deltaTime * rotationSpeed );
        }
    }
}
