﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    [SerializeField] float timeToLive = 1.0f;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        DoTimer2();
    }

    void DoTimer2()
    {
        timeToLive -= Time.deltaTime;
        if ( timeToLive < 0 )
        {
            DestroyBullet();
        }
    }

    private void OnCollisionEnter( Collision collision )
    {
        if ( collision.gameObject.tag == "wallRegular" )
        {
            DestroyBullet();
        }
    }

    private void DestroyBullet()
    {
        Destroy( gameObject );
        if ( GameManager.totalBulletsOnScreen > 0 )
            GameManager.totalBulletsOnScreen--;
    }
}
